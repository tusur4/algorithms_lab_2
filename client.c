#include <string.h>

#include "shared_lib.h"
#include "array_list.h"
#include "linked_list.h"

#define PATH_LENGTH 300

int vertex;
Array_list array;
Array_list path;
bool* visited;


Array_list put_graph_into_array();
bool isConnected(const int n_vertex);
void depth_first_search(int vertex);
int depth_first_search_count(int vertex);
int is_Eulerian(const int n_vertex);
void print_Eulerian_path(const int n_vertex);
void print_Eulerian_path_utility(const int n_vertex, int vertex);
bool is_valid_next_edge(int v1, int v2);
void remove_edge(int v1, int v2);
void add_edge(int v1, int v2);



int main(void)
{
	array = put_graph_into_array();
	const int n_vertex = array_list_amount_elements(array);
	int result;

	if (!n_vertex)
	{
		printf("Graph not found!");
		return 0;
	}

	isConnected(n_vertex);

	printf("\n");

	result = is_Eulerian(n_vertex);
	if (result == 0)
		printf("graph is not Eulerian");
	else if (result == 1)
		printf("graph has a Euler path");
	else
		printf("graph has a Euler cycle");

	printf("\n");

	if (result)
		print_Eulerian_path(n_vertex);

	array_list_destroy(&array);
	return 0;
}

Array_list put_graph_into_array()
{
	char path_file[PATH_LENGTH];
	int i = 0;
	int digit;
	int ch;
	char current_char;
	FILE* my_file;
	bool delimiter;

	array = array_list_create(10);

	Linked_list l = linked_list_create();

	printf("Enter file path: ");
	for (i = 0; i < PATH_LENGTH; i++)
	{
		scanf_s("%c", &current_char);
		if (current_char == '\n')
			break;
		path_file[i] = current_char;
	}
	path_file[i] = '\0';

	fopen_s(&my_file, &path_file[0], "r");

	for (;;)
	{
		ch = fgetc(my_file);
		if (ch != ' ' && ch != '\n')
			break;
	}


	for (delimiter = true;; ch = fgetc(my_file))
	{

		if (ch == '\n' || ch == EOF)
		{
			if (!delimiter)
				linked_list_add(l, vertex);

			if (linked_list_is_empty(l))
				linked_list_destoy(&l);
			else
				array_list_add(&array, l);

			if (ch == '\n')
			{
				l = linked_list_create();
				delimiter = true;
				continue;
			}
			else
				break;
		}


		if (isdigit(ch))
		{
			digit = ch - 48;
			if (delimiter)
				vertex = digit;
			else
				vertex = vertex * 10 + digit;
			delimiter = false;
		}
		else
		{
			if (!delimiter)
			{
				linked_list_add(l, vertex);
			}
			delimiter = true;
		}

	}

	fclose(my_file);

	printf("Entered graph: \n");
	int k = array_list_amount_elements(array);
	for (int j = 0; j < k; j++)
	{
		l = array_list_get(array, j);

		for (i = 0; i < linked_list_get_length(l); i++)
			printf("%d ", linked_list_get_value_with_index(l, i));
		printf("\n");
	}

	return array;
}

void depth_first_search(int vertex)
{
	//Mark current node as visited
	visited[vertex] = true;


	//Recur for all the vertices adjanced to this vertex
	Linked_list l = array_list_get(array, vertex);
	for (int n, i = 0; i < linked_list_get_length(l); i++)
	{
		n = linked_list_get_value_with_index(l, i);
		if (!visited[n])
			depth_first_search(n);
	}
}

int depth_first_search_count(int vertex)
{
	int count = 1;
	visited[vertex] = true;
	Linked_list l = array_list_get(array, vertex);
	for (int n, i = 0; i < linked_list_get_length(l); i++)
	{
		n = linked_list_get_value_with_index(l, i);
		if (!visited[n])
			count += depth_first_search_count(n);

	}
	return count;
}

bool isConnected(const int n_vertex)
{
	int i, j;
	visited = malloc(sizeof(bool) * n_vertex);
	bool result = true;

	for (i = 0; i < n_vertex; i++)
		visited[i] = 0;

	depth_first_search(0);

	for (i = 0; i < n_vertex; i++)
		if (visited[i] == false)
			result = false;

	return result;
}

int is_Eulerian(const int n_vertex)
{
	Linked_list l;
	int  odd;

	if (isConnected(n_vertex) == false)
		return 0;
	odd = 0;
	for (int i = 0; i < n_vertex; i++)
	{
		l = array_list_get(array, i);
		if (linked_list_get_length(l) % 2 != 0)
			odd++;
	}
	if (odd > 2)
		return 0;

	return (odd == 2) ? 1 : 2;

	linked_list_destoy(&l);
}

void print_Eulerian_path(const int n_vertex)
{
	int cur_vertex;
	int next_vertex;
	int* edge_count;
	Linked_list l;
	Linked_list stack;

	printf("Enter a vertex number: ");
	scanf_s("%d", &cur_vertex);

	if (cur_vertex >= n_vertex)
	{
		printf("This vertex does not exist.");
		return;
	}
	cur_vertex;
	path = array_list_create(n_vertex);
	print_Eulerian_path_utility(n_vertex, cur_vertex);

	for (int i = 0; i < array_list_amount_elements(array); i++)
	{
		l = array_list_get(array, i);
		if (linked_list_get_length(l))
		{
			printf("From this point it is impossible to build a Eulerian path!!!");
			return;
		}
	}

	for (int i = 0; i < array_list_amount_elements(path); i++)
	{
		l = array_list_get(path, i);
		printf("%d - %d ", linked_list_get_value_with_index(l, 0), linked_list_get_value_with_index(l, 1));
	}

}

void print_Eulerian_path_utility(const int n_vertex, int vertex)
{
	Linked_list l = array_list_get(array, vertex);

	int vertex2;
	for (int i = 0; i < linked_list_get_length(l); i++)
	{
		vertex2 = linked_list_get_value_with_index(l, i);
		if (is_valid_next_edge(vertex, vertex2, n_vertex))
		{
			//printf("%d - %d ", vertex, vertex2);
			Linked_list cur_l = linked_list_create();
			linked_list_add(cur_l, vertex);
			linked_list_add(cur_l, vertex2);
			array_list_add(&path, cur_l);

			remove_edge(vertex, vertex2);
			print_Eulerian_path_utility(n_vertex, vertex2);
		}

	}

}

bool is_valid_next_edge(int v1, int v2, const int n_vertex)
{
	Linked_list l = array_list_get(array, v1);
	int count1;
	int count2;

	if (linked_list_get_length(l) == 1)
		return true;

	for (int i = 0; i < n_vertex; i++)
		visited[i] = 0;

	count1 = depth_first_search_count(v1);

	remove_edge(v1, v2);

	for (int i = 0; i < n_vertex; i++)
		visited[i] = 0;

	count2 = depth_first_search_count(v1);

	add_edge(v1, v2);

	return(count1 > count2) ? false : true;
}

void remove_edge(int v1, int v2)
{
	Linked_list l;

	l = array_list_get(array, v1);
	linked_list_remove_value(l, v2);
	l = array_list_get(array, v2);
	linked_list_remove_value(l, v1);
}

void add_edge(int v1, int v2)
{
	Linked_list l;

	l = array_list_get(array, v1);
	linked_list_add(l, v2);
	l = array_list_get(array, v2);
	linked_list_add(l, v1);
}