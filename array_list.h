#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H

#include "shared_lib.h"
#include "linked_list.h"

typedef struct array_type* Array_list;

Array_list array_list_create(unsigned int initial_size);
void array_list_add(Array_list* a, Linked_list list);
Linked_list array_list_get(Array_list a,int index);
int array_list_length(Array_list a);
int array_list_amount_elements(Array_list a);
void array_list_make_empty(Array_list* a);
void array_list_destroy(Array_list* a);

#endif
