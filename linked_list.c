#include "linked_list.h"

struct node
{
	Item data;
	struct node* next;
	struct node* previous;
};
struct linked_list_type
{
	struct node* top;
};
void linked_list_set_next(Linked_list l);
void linked_list_set_previous(Linked_list l);
bool linked_list_has_previous(Linked_list l);
void linked_list_set_last(Linked_list l);
static void terminate(const char* message)
{
	printf("%s\n", message);
	exit(EXIT_FAILURE);
}

Linked_list linked_list_create(void)
{
	Linked_list l = malloc(sizeof(struct linked_list_type));
	if (l == NULL)
		terminate("Error in create: stack could not be created.");

	l->top = NULL;

	return l;
}

void linked_list_add(Linked_list l, Item data)
{
	struct node* new_node = malloc(sizeof(struct node));
	if (new_node == NULL)
		terminate("Error in push: stack is full.");
	new_node->data = data;
	new_node->previous = l->top;
	new_node->next = NULL;
	if (l->top != NULL)
	{
		(l->top)->next = new_node;
	}
	l->top = new_node;

}

Item linked_list_get_value(Linked_list l)
{
	if (l == NULL || l->top == NULL)
		terminate("Error in get value from a linked list.");

	return (l->top)->data;

}

Item linked_list_get_value_with_index(Linked_list l, unsigned int index)
{
	int length;
	int rev_index;
	Linked_list top;
	Item value;

	if (l == NULL || l->top == NULL)
		terminate("Error in get value with index from a linked list.");

	length = linked_list_get_length(l);
	if (length <= index)
		terminate("Error in get value with index from a linked list. INDEX grather then the length of the linked list!!!");

	top = l->top;
	rev_index = length - 1 - index;
	for (int i = 0; i != rev_index; i++, linked_list_set_previous(l));
	value = linked_list_get_value(l);
	l->top = top;
	return value;

}

void linked_list_set_next(Linked_list l)
{
	if (l->top != NULL)
		l->top = (l->top)->next;
}

void linked_list_set_previous(Linked_list l)
{
	if (l->top != NULL)
		l->top = (l->top)->previous;
}

bool linked_list_has_previous(Linked_list l)
{
	if (l->top == NULL)
		return false;
	else
		return ((l->top)->previous != NULL);
}

void linked_list_set_last(Linked_list l)
{
	if (l->top == NULL)
		return;

	struct node* last = l->top;

	for (; last->next != NULL;)
		last = last->next;
	l->top = last;
}

int	 linked_list_get_length(Linked_list l)
{
	struct node* top = l->top;
	int i;

	if (linked_list_is_empty(l))
		return 0;

	for (i = 0; !linked_list_is_empty(l); linked_list_set_previous(l), i++);

	l->top = top;

	return i;
}

void linked_list_remove_node(Linked_list l)
{
	if (linked_list_is_empty(l))
		return;

	struct node* top = l->top;
	struct node* previous = top->previous;
	struct node* next = top->next;
	if (previous != NULL)
		previous->next = next;
	if (next != NULL)
	{
		next->previous = previous;
		l->top = next;
	}
	else
		l->top = previous;

	free(top);
	linked_list_set_last(l);

}

bool linked_list_is_empty(Linked_list l)
{
	return (l->top == NULL);
}

void linked_list_remove_value(Linked_list l, Item value)
{
	if (linked_list_is_empty(l))
		return;

	for (int i = 0; i < linked_list_get_length(l); i++)
	{
		if (linked_list_get_value_with_index(l, i) == value)
			linked_list_remove_node_with_index(l, i);
	}

	linked_list_set_last(l);
}

void linked_list_remove_node_with_index(Linked_list l, unsigned int index)
{
	int rev_index;
	int length = linked_list_get_length(l);

	if (length <= index)
		terminate("Error in remove node with index from a linked list.INDEX grather then the length of the linked list!!!");

	rev_index = length - 1 - index;
	for (int i = 0; i != rev_index; i++, linked_list_set_previous(l));

	linked_list_remove_node(l);

}

void linked_list_destoy(Linked_list* l)
{
	struct node* cur_node;
	;

	for (; !linked_list_is_empty(*l);)
	{
		cur_node = (*l)->top;
		linked_list_set_previous(*l);
		free(cur_node);

	}
	free(*l);
	*l = NULL;
}

