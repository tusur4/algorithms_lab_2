#include "array_list.h"

//Struct Hack, content[] - for C99 or content[1] for C89
struct array_type
{
    int len;
    int amount_elements;
    Linked_list content[];
};

static void terminate(const char* message)
{
    printf("%s\n", message);
    exit(EXIT_FAILURE);
}

Array_list array_list_create(unsigned int initial_size)
{

    unsigned int length = sizeof( Linked_list) * initial_size;
    Array_list a = malloc(sizeof(struct array_type) + length);

    if (a == NULL)
        terminate("Error in create Array_list!");
    a->amount_elements = 0;
    a->len = initial_size;

    return a;
}

void array_list_add(Array_list* a,  Linked_list data)
{
    if ((*a)->amount_elements >= (*a)->len)
    {
        (*a)->len += 10;
        *a = realloc((*a), sizeof(struct array_type) + (sizeof( Linked_list)) * ((*a)->len));
        if ((*a) == NULL)
            terminate("Error re-allocate memory!");
    }
    (*a)->content[(*a)->amount_elements++] = data;
}

 Linked_list array_list_get(Array_list a, int index)
{
    return a->content[index];
}

int array_list_length(Array_list a)
{
    return a->len;
}

int array_list_amount_elements(Array_list a)
{
    return a->amount_elements;
}

void array_list_make_empty(Array_list *a)
{
    array_list_destroy(&(*a));
    *a = array_list_create(0);
}

void array_list_destroy(Array_list *a)
{
    free(*a);
}