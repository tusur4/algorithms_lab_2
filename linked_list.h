#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "shared_lib.h"

typedef struct linked_list_type* Linked_list;

Linked_list linked_list_create(void);
void linked_list_add(Linked_list l, Item data);
Item linked_list_get_value(Linked_list l);
Item linked_list_get_value_with_index(Linked_list l, unsigned int index);
int	 linked_list_get_length(Linked_list l);
bool linked_list_is_empty(Linked_list l);
void linked_list_remove_node(Linked_list l);
void linked_list_remove_node_with_index(Linked_list l, unsigned int index);
void linked_list_remove_value(Linked_list l, Item value);
void linked_list_destoy(Linked_list l);

#endif
